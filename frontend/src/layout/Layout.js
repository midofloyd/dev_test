import React, { useState } from 'react'
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom"
import { Layout, Menu } from 'antd'
import {
  UserOutlined,
  BookOutlined,
  ContactsOutlined,
} from '@ant-design/icons'
import logo from '../images/logo/automizy-color-logo-full.svg'
import "./Layout.css"
import Student from '../page/student/Student'
import Project from '../page/project/Project'
import Management from '../page/management/Management'
import ManagementDetail from '../page/management/ManagementDetail/ManagementDetail'

import axios from 'axios'
axios.defaults.baseURL = 'http://localhost:3000/'

const App = () => {

  const pages = [
    { name: 'Student', icon: <UserOutlined /> },
    { name: 'Project', icon: <BookOutlined />},
    { name: 'Management', icon: <ContactsOutlined />}
  ]

  const [collapsed, setCollapsed] = useState(false)
  const [activeMenu, setActiveMenu] = useState('student')

  const handleOnClickMenu = e => {
    setActiveMenu(e.key)
  }

  const menuItems = pages.map(page => {
    return (
      <Menu.Item key={page.name.toLowerCase()} icon={page.icon}>
        <Link to={'/' + page.name.toLowerCase()} >{page.name}</Link>
      </Menu.Item>
    )
  })

  return (
    <Router>
      <Layout className="layout">
        <Layout.Sider 
          trigger={null} 
          collapsible 
          collapsed={collapsed}
          className={"sider"}
          >
          <img 
            className={collapsed ? "logo-hide" : "logo"}
            src={logo}
            alt={""}/>
          <Menu 
            theme="dark" 
            mode="inline"
            selectedKeys={[activeMenu]}
            onClick={ e => handleOnClickMenu(e) }>
              {menuItems}
          </Menu>
        </Layout.Sider>
        <Switch>
          <Route exact path="/">
            <Student/>
          </Route>
          <Route path="/student">
            <Student/>
          </Route>
          <Route path="/project">
            <Project/>
          </Route>
          <Route exact path="/management">
            <Management/>
          </Route>
          <Route path="/management/:id">
            <ManagementDetail />
          </Route>
        </Switch>
      </Layout>
    </Router>
  )
}
export default App