import React from 'react'
import { Layout, Row, Col, Typography, Button } from 'antd'
const { Header } = Layout
const { Title } = Typography

const ModulHeader = ({ title, onClickAddNewItem, button, buttonName }) => {
  return (
    <Header className="header">
      <Row>
        <Col span={22}>
          <Title>{title}</Title>
        </Col>
        {button ?
          <Col span={2}>
            <Button
              type="primary"
              onClick={onClickAddNewItem}>
              {buttonName}
            </Button>
          </Col>
          :
          null
        }
      </Row>
    </Header>
  )
}

export default ModulHeader