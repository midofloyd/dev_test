import React, { useState } from 'react'
import { Alert, Modal, Button, Typography, List, Row, Col } from 'antd'

const AlertMessage = ({ message, type, errors, onClose }) => {

    const [isModalVisible, setIsModalVisible] = useState(false);

    const showModal = () => {
        setIsModalVisible(true);
    };

    const handleOk = () => {
        setIsModalVisible(false);
    };

    const handleCancel = () => {
        setIsModalVisible(false);
    };

    return (
        <>
            <Alert
                message={message}
                type={type}
                closable
                onClose={onClose}
                action={
                    <Button size="small" type="text" onClick={showModal}>
                        Details
                </Button>
                }
            />
            <Modal
                visible={isModalVisible}
                onOk={handleOk}
                onCancel={handleCancel}
                title="Error Detail"
                >
                <Row style={{ marginTop: 8, marginBottom: 8 }}>
                    <Col span={24}>
                        {errors.length ?
                            <List
                                bordered
                                dataSource={errors}
                                renderItem={item => (
                                    <List.Item>
                                        <Typography.Text strong>
                                            {item}
                                        </Typography.Text>
                                    </List.Item>
                                )}
                            >
                            </List>
                            :
                            null
                        }
                    </Col>
                </Row>
            </Modal>
        </>
    )
}

export default AlertMessage