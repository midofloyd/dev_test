import React, {useState} from 'react'
import { Layout, message } from 'antd'
const { Content } = Layout
import "../../layout/Layout.css"

import ModulHeader from '../components/ModulHeader/ModulHeader'
import ListProject from './ListProject/ListProject'
import AddProjectModal from './AddProjectModal/AddProjectModal'

const Project = () => {

  const [reloadListTrigger, setReloadListTrigger] = useState(null)
  const [showModal, setShowModal] = useState(false)

  // Új project hozzáadása gombra kattintás
  const onClickAddNewProject = () =>{
      setShowModal(true)
  }
  const onClickCancel=()=>{
      setShowModal(false)
  }
  const onDone=({name})=>{
      setShowModal(false)
      setReloadListTrigger(new Date().getTime())
      message.success('The following project has been saved: ' + name)
  }

  return (
    <Layout>
      <ModulHeader title="Project Handler" onClickAddNewItem={onClickAddNewProject} button buttonName="Add New Project"/>
      <Content className="content">
        <ListProject reloadListTrigger={reloadListTrigger}/>
        <AddProjectModal visible={showModal} onClickCancel={onClickCancel} onDone={onDone}/>
      </Content>
    </Layout>
  )
}

export default Project