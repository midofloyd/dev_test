import React, { useState } from 'react'
import { Layout, message } from 'antd'
const { Content } = Layout
import "../../layout/Layout.css"

import ModulHeader from '../components/ModulHeader/ModulHeader'
import ListStudent from './ListStudent/ListStudent'
import AddStudentModal from './AddStudentModal/AddStudentModal'

const Student = () => {

    const [reloadListTrigger, setReloadListTrigger] = useState(null)
    const [showModal, setShowModal] = useState(false)

    // Új tanuló hozzáadása gombra kattintás
    const onClickAddNewStudent = () => {
        setShowModal(true)
    }
    const onClickCancel = () => {
        setShowModal(false)
    }
    const onDone = ({ name }) => {
        setShowModal(false)
        setReloadListTrigger(new Date().getTime())
        message.success('The following student has been saved: ' + name)
    }
    
    return (
        <Layout>
            <ModulHeader title="Student Handler" onClickAddNewItem={onClickAddNewStudent} button buttonName="Add New Student"/>
            <Content className="content">
                <ListStudent reloadListTrigger={reloadListTrigger} />
                <AddStudentModal visible={showModal} onClickCancel={onClickCancel} onDone={onDone} />
            </Content>
        </Layout>
    )
}

export default Student