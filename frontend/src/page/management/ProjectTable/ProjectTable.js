import React, { useState, useEffect } from 'react'
import {
    Link
} from "react-router-dom"
import axios from 'axios'
import {
    Row,
    Col,
    Tag,
    Tooltip,
    Table,
    Modal,
    message
} from 'antd'
const { confirm } = Modal
import { ExclamationCircleOutlined } from '@ant-design/icons'

import ExpandedTableRow from './ExpandedTableRow/ExpandedTableRow'

const ProjectTable = () => {

    const [trigger, setTrigger] = useState()
    const [loader, setLoader] = useState(true)

    const [list, setList] = useState({
        data: null,
        complete: false,
        error: false
    })

    // GET Project with Students
    useEffect(
        () => {
            setLoader(true)
            setList({
                data: list.data,
                error: false,
                complete: false
            })
            axios.get('api/managements/project')
                .then(res => {
                    setLoader(false)
                    setList({
                        data: res.data.managements,
                        error: false,
                        complete: true
                    })
                }
                )
                .catch(() => {
                    setLoader(false)
                    setList({
                        data: null,
                        error: true,
                        complete: true
                    })
                }
                )
        },
        [trigger]
    )

    const onClickRemoveFromProject = ({ name, project_id, student_id }) => {
        confirm({
            title: 'Are you sure remove this student?',
            icon: <ExclamationCircleOutlined />,
            content: name,
            okText: 'Yes',
            okType: 'danger',
            cancelText: 'No',
            onOk() {
                removeStudentFromProject({ name, project_id, student_id })
            },
            onCancel() { }
        })
    }

    // remove Student from project
    const removeStudentFromProject = ({ name, project_id, student_id }) => {
        setLoader(true)
        axios.delete('api/managements/project/' + project_id + '/student/' + student_id)
            .then(res => {
                message.success('The following student has been removed from the project: ' + name)
                setLoader(false)
                setTrigger(new Date().getTime())
            }
            )
            .catch(() =>{
                message.error('Error during remove the Student')
                setLoader(false)
                }
            )
    }

    // Project table columns names
    const columns = [
        {
            title: 'Project Name',
            dataIndex: 'name',
            key: 'name',
        },
        {
            title: 'Description',
            dataIndex: 'desc',
            key: 'desc',
        },
        {
            title: 'Students',
            dataIndex: 'students',
            key: 'students',
            render: students =>
            (
                <Tooltip placement="topLeft" title='Project Participants'>
                    <Tag color={students.length ? "green" : "red"}>Participants: {students.length}</Tag>
                </Tooltip>
            )
        },
        {
            title: 'Action',
            key: 'action',
            render: record => (
                <Link to={'/management/' + record.id}>
                    Add Student
                </Link>
            )
        },
    ];

    // render for expanded table
    const expandable = {
        expandedRowRender: record => (
            <ExpandedTableRow
                rowData={record.students}
                projectId={record.id}
                onClickDelete={({ name, project_id, student_id }) => onClickRemoveFromProject({ name, project_id, student_id })}
            />
        )
    }

    return (
        <Row style={{ marginTop: 8, marginBottom: 8 }}>
            <Col span={24}>
                <Table
                    loading={loader}
                    dataSource={list.data}
                    rowKey="id"
                    columns={columns}
                    expandable={expandable}
                    bordered
                />
            </Col>
        </Row>
    )
}

export default ProjectTable