import React from 'react'
import { Table, Typography, Button } from 'antd'

const ExpandedTableRow = ({ projectId, rowData, onClickDelete }) => {

    // Student columns name
    const expandedColumns = [
        {
            title: 'Student Name',
            key: 'first_name',
            render: record => <Typography.Text strong>{record.first_name} {record.last_name}</Typography.Text>
        },
        {
            title: 'Email',
            key: 'email',
            dataIndex: 'email'
        },
        {
            title: 'Action',
            key: 'action',
            render: record => (
                <Button
                    type="primary"
                    onClick={({ name = record.first_name + " " + record.last_name, project_id = projectId, student_id = record.id }) => onClickDelete({ name, project_id, student_id })} >
                    Remove
                </Button>
            )
        }
    ]

    return (
        < Table
            rowKey="id"
            dataSource={rowData}
            columns={expandedColumns}
            pagination={false}
            bordered
            size="small"
        />
    )
}

export default ExpandedTableRow