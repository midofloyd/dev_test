import React, { useState } from 'react'
import axios from 'axios'
import { Row, Col, Card, Transfer, Spin, Empty, message } from 'antd'

import AlertMessage from '../../../components/AlertMessage/AlertMessage'

const StudentTransfer = ({ studentList, project, loading, onDone }) => {

    const [transferDisable, setTransferDisable] = useState(false)
    const [errorCollector, setErrorCollector] = useState([])

    const transferHandler = (targetKeys, direction, moveKeys) => {
        setErrorCollector([])
        setTransferDisable(true)

        // remove direction in Transfer component
        if (direction === 'left') {
            removeStudent(moveKeys[0])
        }

        // add direction in Transfer component
        if (direction === 'right') {
            Promise.all(addHandler(moveKeys))
                .then(responses => {
                    message.success('Students have been added to the Project')
                })
                .catch(err => {
                    message.error('Adding was not success.')
                })
                .finally(() => {
                    onDone()
                    setTransferDisable(false)
                })
        }
    }

    // delete request for remove 1 student from the project
    const removeStudent = studentId => {
        axios.delete('api/managements/project/' + project.data.id + '/student/' + studentId)
            .then(res => {
                const student = studentList.data.students.find(({ id }) => id === studentId)
                if (res.status === 204) {
                    message.info(`${student.first_name + " " + student.last_name} were not memeber of the Project`)
                } else {
                    message.success(`${student.first_name + " " + student.last_name} has been removed from the Project`)
                }
            })
            .catch(err => {
                message.error('Removing was not success.')
            })
            .finally(() => {
                onDone()
                setTransferDisable(false)
            })
    }

    // post request for add 1 student to the project
    const addStudent = studentId => {
        return axios.post('api/managements/project/' + project.data.id + '/student', { "id": studentId })
            .then(res => res)
            .catch(err => {
                if (err.response.status === 409) {
                    const student = studentList.data.students.find(({ id }) => id === studentId)
                    const errorMessage = `${student.first_name} ${student.last_name} is already member of the project`
                    setErrorCollector(prevState => [...prevState, errorMessage])
                } else {
                    return err.response
                }
            })
    }

    // add students based on the given ID list
    const addHandler = (studentIdList) => {
        const requestList = []
        studentIdList.forEach(studentId => {
            requestList.push(addStudent(studentId))
        })
        return requestList
    }

    const alertClose = () => {
        setErrorCollector([])
    }

    return (
        <Card title="Participants" style={{ width: 600 }}>
            <Row>
                <Col span={24} style={{ marginTop: 8, marginBottom: 8 }}>
                    <Spin
                        size="large"
                        spinning={loading}>
                        {(studentList.complete && project.complete && (
                            project.data && studentList.data.students ?
                                <Transfer
                                    titles={['Students', 'Participants']}
                                    oneWay
                                    dataSource={studentList.data.students}
                                    targetKeys={project.data.students}
                                    onChange={transferHandler}
                                    disabled={transferDisable}
                                    rowKey={record => record.id}
                                    render={item => item.first_name + " " + item.last_name}
                                />
                                :
                                <Empty />
                        ))}
                    </Spin>
                </Col>
                {errorCollector.length ?
                <Col span={24} style={{ marginTop: 8, marginBottom: 8 }}>
                    <AlertMessage
                        type="warning"
                        message="Warning notice about adding." 
                        errors={errorCollector}
                        onClose={alertClose}
                    />
                </Col>
                    : null
                }
            </Row>
        </Card>
    )
}

export default StudentTransfer