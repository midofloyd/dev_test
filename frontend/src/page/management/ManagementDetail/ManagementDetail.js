import React, { useState, useEffect } from 'react'
import { useHistory, useParams } from 'react-router-dom'
import axios from 'axios'
import { Layout, PageHeader, Row, Col, Spin, Empty } from 'antd'
const { Content } = Layout

import ModulHeader from '../../components/ModulHeader/ModulHeader'
import StudentTransfer from './StudentTransfer/StudentTransfer'

const ManagementDetail = () => {
  const history = useHistory()
  //id for READ management api/management/:id
  let { id } = useParams();

  const [trigger, setTrigger] = useState()
  const [loader, setLoader] = useState(true)

  const [project, setProject] = useState({
    data: null,
    complete: false,
    error: false
  })

  const [studentsList, setStudentsList] = useState({
    data: null,
    complete: false,
    error: false
  })

  useEffect(
    () => {
      setLoader(true)

      const projectDetail = getProjectDetail()
      const students = getStudentsList()

      Promise.all([projectDetail, students])
        .finally(() => {
          setLoader(false)
        })
    },
    [trigger]
  )

  // get project with students  api/management/:id
  const getProjectDetail = () => {
    setProject({
      data: project.data,
      error: false,
      complete: false
    })
    return axios.get('api/managements/project/' + id)
      .then(res => {
        // for Transfer components
        // array of students id [1,2,3,4]
        const responseData = res.data
        const students = responseData.students.map(student => student.id)
        responseData.students = students

        setProject({
          data: responseData,
          error: false,
          complete: true
        })
      }
      )
      .catch(() => {
        setProject({
          data: null,
          error: true,
          complete: true
        })
      }
      )
  }

  // get all students
  const getStudentsList = () => {
    setStudentsList({
      data: studentsList.data,
      error: false,
      complete: false
    })
    return axios.get('api/student')
      .then(res => {
        setStudentsList({
          data: res.data,
          error: false,
          complete: true
        })
      }
      )
      .catch(() => {
        setStudentsList({
          data: null,
          error: true,
          complete: true
        })
      }
      )
  }

  //back button handler to main management page
  const onBack = () => {
    history.push("/management")
  }

  const onDone = () => {
    setTrigger(new Date().getTime())
  }

  return (
    <Layout>
      <ModulHeader title="Management Detail" />
      <Content className="content">
        <Row style={{ marginTop: 0, marginBottom: 8 }}>
          <Col span={24}>
            <PageHeader
              onBack={onBack}
              style={{ paddingLeft: 0, paddingTop: 0, paddingBottom: 24 }}
              title="Back"
            />
          </Col>
          <Col span={24}>
            <StudentTransfer
              loading={loader}
              studentList={studentsList}
              project={project}
              onDone={onDone}
            />
          </Col>
        </Row>
      </Content>
    </Layout>
  )
}

export default ManagementDetail