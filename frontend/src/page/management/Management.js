import React from 'react'
import { Layout } from 'antd'
const { Content } = Layout

import ModulHeader from '../components/ModulHeader/ModulHeader'
import ProjectTable from './ProjectTable/ProjectTable'

const Management = () => {
  return (
    <Layout>
      <ModulHeader title="Management Handler" />
      <Content className="content">
        <ProjectTable />
      </Content>
    </Layout>
  )
}

export default Management