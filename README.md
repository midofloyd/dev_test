# Automizy Dev Test
## Frontend

cd frontend

### Docker

docker-compose build --no-cache

docker-compose up

http://localhost/

### Development

yarn install

yarn start

http://localhost:8080/

## Backend

cd backend

### Docker

docker-compose build --no-cache

docker-compose up

### Development


yarn install

yarn start:rest

yarn start:micro

## Arhitektúra felépítése
---
![alt text](https://bitbucket.org/automizy-public/dev-test/raw/7b1eee7c38731d438707982520c2be87bd0534d3/doc/image/arhitecture.png "Arhitect")
## Adatbázis felépítése
---
![alt text](https://bitbucket.org/automizy-public/dev-test/raw/967d17b253844231c3e9d2482f0d9f62e519f448/doc/image/database.png "Database")
