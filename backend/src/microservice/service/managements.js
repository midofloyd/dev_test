import path from 'path'
import grpc from 'grpc'
const protoLoader = require("@grpc/proto-loader")
import config from '../../config/service'
import db from '../../microservice/database/connect'
import ManagementsModel from '../database/model/managements'
import ProjectModel from '../database/model/project'
import StudentModel from '../database/model/student'

const PROTO_PATH = path.join(__dirname, '../../proto/managements.proto')

const packageDefinition = protoLoader.loadSync(PROTO_PATH, {
    keepCase: true,
    longs: String,
    enums: String,
    defaults: true,
    oneofs: true
})

// Load in our service definition
const managementsProto = grpc.loadPackageDefinition(packageDefinition).managements
const server = new grpc.Server()

const projectModel = ProjectModel(db)
const studentModel = StudentModel(db)
const mngmtsModel = ManagementsModel(db)

mngmtsModel.associate(projectModel, studentModel, 'project_id')
mngmtsModel.associate(studentModel, projectModel, 'student_id')

// Implement the list function
// Get all Projects with Students
const List = async (call, callback) => {
    try {
        const result = await projectModel.findAll({
            include: [{
                model: studentModel,
                as: 'students'
            }]
        })
        callback(null, { managements: result })
    }
    catch (err) {
        callback({
            code: grpc.status.ABORTED,
            details: "Aborted"
        })
    }
}

// Implement the read function
// Project list by ID with Students
const Read = async (call, callback) => {
    let id = call.request.id
    try{
        let result = await projectModel.findByPk(id,
            {
                include: [{
                    model: studentModel,
                    as: 'students'
            }]
        })
        if(result){
            callback(null, result)
        }
        else{
            callback({
                code: grpc.status.NOT_FOUND,
                details: "Not found"
            })
        }
    }catch(err){
        callback({
            code: grpc.status.ABORTED,
            details: "Aborted"
        })
    }
}

// Implement insert function
// Post Student to Project
const Create = async (call, callback) => {
    let project_id = call.request.project_id
    let student_id = call.request.student_id
    try {
        // check project is exist
        let project = await projectModel.findByPk(project_id)
        if (!project) {
            callback({
                code: grpc.status.NOT_FOUND,
                details: "Not found"
            })
        } else {
            // check student is exist
            let student = await studentModel.findByPk(student_id)
            if(!student) {
                callback({
                    code: grpc.status.NOT_FOUND,
                    details: "Not found"
                })
            } else {            
                let result = await mngmtsModel.create({
                    project_id,
                    student_id
                })
                callback(null, { 'id': result.id })
            }
        }
    }
    catch(err) {
        switch(err.name) {
            case 'SequelizeUniqueConstraintError':
                let uniqueErr = new Error('ALREADY_EXISTS')
                uniqueErr.code = grpc.status.ALREADY_EXISTS
                uniqueErr.metadata = dbErrorCollector({errors: err.errors})
                callback(uniqueErr)
                break
            case 'SequelizeForeignKeyConstraintError':
                let fKeyErr = new Error('NOT_FOUND')
                fKeyErr.code = grpc.status.NOT_FOUND
                const metadata = new grpc.Metadata()
                const message = err.fields.map(item => {
                    metadata.set(item, "not found with that ID")
                })
                fKeyErr.metadata = metadata
                callback(fKeyErr)
                break
            default:
                callback({
                    code: grpc.status.ABORTED,
                    details: "ABORTED"
                })
        }
    }
}

// Implement delete function
// Student remove from Project
const Delete = async (call, callback) => {
    let project_id = call.request.project_id
    let student_id = call.request.student_id
    try {
        let result = await mngmtsModel.destroy(
            { where: {
                project_id: project_id,
                student_id: student_id
            }
        })
        if(result){
            callback(null, { 'id': student_id })
        }
        else{
            callback({
                code: grpc.status.NOT_FOUND,
                details: "Not found"
            })
        }
    }
    catch(err) {
        console.log(err, err.name);
        callback({
            code: grpc.status.ABORTED,
            details: "ABORTED"
        })
    }
}

// Collect errors
const dbErrorCollector = ({
    errors
}) => {
    const metadata = new grpc.Metadata()
    const error = errors.map(item => {
        metadata.set(item.path, item.type)
    })
    return metadata
}
const exposedFunctions = {
    List,
    Read,
    Create,
    Delete
}

server.addService(managementsProto.ManagementsService.service, exposedFunctions)
server.bind(config.managements.host + ':' + config.managements.port, grpc.ServerCredentials.createInsecure())

db.sequelize.sync().then(() => {
    console.log("Re-sync db.")
    server.start()
    console.log('Server running at ' + config.managements.host + ':' + config.managements.port)
})
    .catch(err => {
        console.log('Can not start server at ' + config.managements.host + ':' + config.managements.port)
        console.log(err)
    })