const ManagementsModel = ({
    sequelize, 
    DataType
  }) => {
  const {INTEGER, DATE, NOW} = DataType
  const Managements = sequelize.define("managements", {
    id: {
      type: INTEGER, 
      primaryKey: true, 
      autoIncrement: true
    },
    createdAt: {
      type: DATE,
      defaultValue: NOW
    }
  })
  // function to define N:M associations
  Managements.associate = (firstModel, secondModel, fKey) => {
    firstModel.belongsToMany(secondModel, {
      through: Managements,
      foreignKey: fKey
    })
  }

  return Managements;
}
  
export default ManagementsModel