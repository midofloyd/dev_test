import { body, validationResult } from 'express-validator'
import path from 'path'
import grpc from 'grpc'
const protoLoader = require("@grpc/proto-loader")
import config, { student } from '../../config/service'
const PROTO_PATH = path.join(__dirname, '../../proto/managements.proto')

exports.validationRules = (method) => {
    switch (method) {
        case 'create': {
            return [
                body('id').not().isEmpty()
            ]
        }
    }
}

exports.validate = (req, res, next) => {
    const errors = validationResult(req)
    if (errors.isEmpty()) {
        return next()
    }
    const extractedErrors = []
    errors.array().map(err => extractedErrors.push({ [err.param]: err.msg }))
  
    return res.status(400).json({
        errors: extractedErrors
    })
}

const packageDefinition = protoLoader.loadSync(PROTO_PATH, {
    keepCase: true,
    longs: String,
    enums: String,
    defaults: true,
    oneofs: true
})

// Load in our service definition
const managementsProto = grpc.loadPackageDefinition(packageDefinition).managements
const client = new managementsProto.ManagementsService(config.managements.host +':'+ config.managements.port, grpc.credentials.createInsecure())

// Get All projects with students
const getProjectsStudents = (options) => {
    return new Promise((resolve, reject) => {
        client.List(options, (error, response) => {
              if (error) { reject(error) }
              resolve(response)
          })
      })
}

exports.listProject = async (req, res, next) => {
    try{
        let result = await getProjectsStudents()
        res.status(200).json(result)
    } catch(e){
        res.json(e)
    }
}

// Get project by ID with students
const getProjectListofStudents = (options) => {
    return new Promise((resolve, reject) => {
        client.Read(options, (error, response) => {
            if (error) { reject(error) }
            resolve(response)
        })
    })
}

exports.readProject = async (req, res, next) => {
    try{
        let result = await getProjectListofStudents({
            "id": req.params.projectId
        })
        res.status(200).json(result)
    } catch(e){
        if(e.details === 'Not found'){
            res.status(204).json(e)
        }
        else{
            res.status(500).json(e)
        }
    }
}

// Post student to project
const postStudentToProject = (options) => {
    return new Promise((resolve, reject) => {
      client.Create(options, (error, response) => {
            if (error) { reject(error) }
            resolve(response)
        })
    })
}

exports.postStudent = async (req, res, next) => {
    // api/managements/:projectId/student/
    try {
        let result = await postStudentToProject({
            "student_id": req.body.id,
            "project_id": req.params.projectId
        })
        res.status(201).json(result)
    } 
    catch(err){
        switch(err?.details){
            case 'ALREADY_EXISTS':
                res.status(409).json({
                    error: err.metadata.getMap()
                })
                break
            case 'Not found':
                res.status(204).json(err)
                break
            default:
                res.status(500).json(err)
        }
    }
}

// Delete student from project
const deleteStudentFromProject = (options) => {
    return new Promise((resolve, reject) => {
      client.Delete(options, (error, response) => {
            if (error) { reject(error) }
            resolve(response)
        })
    })
}

exports.deleteStudent = async (req, res, next) => {
    // api/managements/:projectId/student/:studentId
    try {
        let result = await deleteStudentFromProject({
            "project_id": req.params.projectId,
            "student_id": req.params.studentId
        })
        res.status(200).json(result)
    } 
    catch(err){
        if(err.details === 'Not found'){
            res.status(204).json(err)
        }
        else{
            res.status(500).json(err)
        }
    }
}