import express from 'express'
const router = express.Router()
import managements from '../controller/managements'

// Classic CRUD solution
// Function	    Request Method
// list	        GET
// get	        GET
// create	    POST
// update	    PUT
// delete	    DELETE
// set	        PATCH

// GET request for list of all items
router.get('/project', managements.listProject)
// GET request for read an item by id
router.get('/project/:projectId', managements.readProject)
// POST request for add student to project
router.post('/project/:projectId/student', managements.validationRules('create'), managements.validate, managements.postStudent)
// DELETE request for delete student from project
router.delete('/project/:projectId/student/:studentId', managements.deleteStudent)

export default router