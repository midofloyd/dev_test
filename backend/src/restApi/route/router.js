import express from 'express'
const router = express()
import student from './student'
import project from './project'
import managements from './managements'

// Documentation
// https://expressjs.com/en/api.html#router

// Hallgatókat kezelő útvonalak
router.use('/student', student)

// Project kezelő útvonalak
router.use('/project', project)

//Managements kezelő utak
router.use('/managements', managements)

export default router