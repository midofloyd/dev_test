module.exports={
    student: {
        'host': process.env.SERVICE_STUDENT_HOST || '0.0.0.0',
        'port': process.env.SERVICE_STUDENT_PORT || 50053
    },
    project: {
        'host': process.env.SERVICE_PROJECT_HOST || '0.0.0.0',
        'port': process.env.SERVICE_PROJECT_PORT || 50054
    },
    managements: {
        'host': process.env.SERVICE_MANAGEMENT_HOST || '0.0.0.0',
        'port': process.env.SERVICE_MANAGEMENT_PORT || 50055
    }
}