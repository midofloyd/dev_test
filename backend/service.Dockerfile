FROM node:14

WORKDIR /app

COPY ./ /app

RUN yarn

#script to wait for database 
RUN chmod +x ./wait-for-it.sh
